$(function(){

    function BuscaHandleEquipes() {
        var ListEquipes = document.getElementById('EquipeID');
        var HandleEquipes = "";

        if (ListEquipes.length == 0) {
            HandleEquipes = "-1"
        } else {
            for (var i = 0; i < ListEquipes.length; i++) {

                if (ListEquipes[i].value == "-1") {
                    HandleEquipes = "-1";
                    break;
                }
                if (i == ListEquipes.length - 1) {
                    HandleEquipes += ListEquipes[i].value;
                } else {
                    HandleEquipes += ListEquipes[i].value + ",";
                }
            }
        }
        return HandleEquipes;
    }

    function BuscaHandleClientes() {
        var ListClientes = document.getElementById('ClienteID');
        var HandleClientes = "";

        if (ListClientes.length == 0) {
            HandleClientes = "-1"
        } else {

            for (var i = 0; i < ListClientes.length; i++) {

                if (ListClientes[i].value == "-1") {
                    HandleClientes = "-1";
                    break;
                }
                if (i == ListClientes.length - 1) {
                    HandleClientes += ListClientes[i].value;
                } else {
                    HandleClientes += ListClientes[i].value + ",";
                }
            }
        }
        return HandleClientes;
    }

    function BuscaHandleProdutos() {
        var ListProdutos = document.getElementById('ProdutoID');
        var HandleProdutos = "";

        if (ListProdutos.length == 0) {
            HandleProdutos = "-1"
        } else {

            for (var i = 0; i < ListProdutos.length; i++) {
                if (ListProdutos[i].value == "-1") {
                    HandleProdutos = "-1";
                    break;
                }
                if (i == ListProdutos.length - 1) {
                    HandleProdutos += ListProdutos[i].value;
                } else {
                    HandleProdutos += ListProdutos[i].value + ",";
                }
            }
        }

        return HandleProdutos;
    }

    function AtualizarCads() {
        var erro = false;
        var produtos = BuscaHandleProdutos();
        var clientes = BuscaHandleClientes();
        var equipes = BuscaHandleEquipes();

        // NOVAS ABERTAS E J� LIBERADAS
        $.ajax({
            url: "/Home/CountNovasSMSAbertasEEncerradas/",
            dataType: 'json',
            data: { 'sistemas': produtos, 'clientes': clientes, 'equipes': equipes },
        }).done(function (msg) {
            $(msg).each(function (i) {
                $('#valorNovasAbertas').text(msg[i].QuantNovasSMS);
                $('#valorLiberadas').text(msg[i].QuantSMSLiberadas);
            });
        }).fail(function (jqXHR, textStatus, msg) {
            erro = true;
            });

        // SLA E MONTAGEM DOS GRAFICOS
        $.ajax({
            url: "/Home/BuscaPercentualSLA/",
            dataType: 'json',
            data: { 'sistemas': produtos, 'clientes': clientes },
        }).done(function (msg) {
            $(msg).each(function (i) {
                $('#ValorSLA').text(msg[i].Sla);
            });
        }).fail(function (jqXHR, textStatus, msg) {
            erro = true;
            });

        // ABERTAS
        $.ajax({
            url: "/Home/CountSMSAbertas/",
            dataType: 'json',
            data: { 'sistemas': produtos, 'clientes': clientes, 'equipes': equipes },
        }).done(function (msg) {
            $(msg).each(function (i) {
                $('#valorAbertas').text(msg[i].Quantidade);
            });
        }).fail(function (jqXHR, textStatus, msg) {
            erro = true;
            });

        // CORRE�OES
        $.ajax({
            url: "/Home/CountSMSCorrecoes/",
            dataType: 'json',
            data: { 'sistemas': produtos, 'clientes': clientes, 'equipes': equipes },
        }).done(function (msg) {
            $(msg).each(function (i) {
                $('#valorCorrecoes').text(msg[i].Quantidade);
            });
        }).fail(function (jqXHR, textStatus, msg) {
            erro = true;
            });

        // VENCIDAS ABERTAS
        $.ajax({
            url: "/Home/CountSMSVencidasAbertas/",
            dataType: 'json',
            data: { 'sistemas': produtos, 'clientes': clientes, 'equipes': equipes },
        }).done(function (msg) {
            $(msg).each(function (i) {
                $('#valorAbertasVencidas').text(msg[i].Quantidade);
                $('#TotalizadorSMSVencidas').text("Total: " + msg[i].Quantidade);
            });
        }).fail(function (jqXHR, textStatus, msg) {
            erro = true;
            });

        // VENCENDO
        $.ajax({
            url: "/Home/CountSMSVencendo/",
            dataType: 'json',
            data: { 'sistemas': produtos, 'clientes': clientes, 'equipes': equipes },
        }).done(function (msg) {
            $(msg).each(function (i) {
                $('#TotalizadorSMSVencendo').text("Total: " + msg[i].Quantidade);
            });
        }).fail(function (jqXHR, textStatus, msg) {
            erro = true;
            });

        if (erro) {
            alert("Erro ao consultar !");
        }
    }

    function LimpaTabelaVencidas() {
        $("#BodyTabelaSMSVencidas tr").remove();
    }

    function LimpaTabelaVencendo() {
        $("#BodyTabelaSMSVencendo tr").remove();
    }

    function AtualizarGridVencidasAbertas() {
        var tbody = document.querySelector("#BodyTabelaSMSVencidas");
        LimpaTabelaVencidas();
        $.ajax(
            {
                url: "/Home/ListarSMSVencidasAbertas/",
                dataType: 'json',
                data: { 'sistemas': BuscaHandleProdutos(), 'clientes': BuscaHandleClientes(), 'equipes': BuscaHandleEquipes() },
                success: function (response) {
                    $(response).each(function (i) {
                        var tr = document.createElement("tr");

                        var sms = document.createElement("td");
                        sms.textContent = response[i].Sms;

                        var sigla = document.createElement("td");
                        sigla.textContent = response[i].Sigla;

                        var classificacao = document.createElement("td");
                        classificacao.textContent = response[i].Classificacao;

                        var Severidade = document.createElement("td");
                        Severidade.textContent = response[i].Severidade;

                        var Situacao = document.createElement("td");
                        Situacao.textContent = response[i].Situacao;

                        var Equipe = document.createElement("td");
                        Equipe.textContent = response[i].Equipe;

                        var Responsavel = document.createElement("td");
                        Responsavel.textContent = response[i].Responsavel;

                        var DiasVencidos = document.createElement("td");
                        DiasVencidos.textContent = response[i].DiasVencidos;

                        tr.appendChild(sms);
                        tr.appendChild(sigla);
                        tr.appendChild(classificacao);
                        tr.appendChild(Severidade);
                        tr.appendChild(Situacao);
                        tr.appendChild(Equipe);
                        tr.appendChild(Responsavel);
                        tr.appendChild(DiasVencidos);

                        tbody.appendChild(tr);
                    });
                }
            }
        );
    }

    function AtualizarGridVencendo() {
        var tbody = document.querySelector("#BodyTabelaSMSVencendo");
        LimpaTabelaVencendo();
        $.ajax(
            {
                url: "/Home/ListarSMSVencendo/",
                dataType: 'json',
                data: { 'sistemas': BuscaHandleProdutos(), 'clientes': BuscaHandleClientes(), 'equipes': BuscaHandleEquipes() },
                success: function (response) {
                    $(response).each(function (i) {
                        var tr = document.createElement("tr");

                        var sms = document.createElement("td");
                        sms.textContent = response[i].Sms;

                        var sigla = document.createElement("td");
                        sigla.textContent = response[i].Sigla;

                        var classificacao = document.createElement("td");
                        classificacao.textContent = response[i].Classificacao;

                        var Severidade = document.createElement("td");
                        Severidade.textContent = response[i].Severidade;

                        var Situacao = document.createElement("td");
                        Situacao.textContent = response[i].Situacao;

                        var Equipe = document.createElement("td");
                        Equipe.textContent = response[i].Equipe;

                        var Responsavel = document.createElement("td");
                        Responsavel.textContent = response[i].Responsavel;

                        var DiasPVencer = document.createElement("td");
                        DiasPVencer.textContent = response[i].DiasParaVencer;

                        tr.appendChild(sms);
                        tr.appendChild(sigla);
                        tr.appendChild(classificacao);
                        tr.appendChild(Severidade);
                        tr.appendChild(Situacao);
                        tr.appendChild(Equipe);
                        tr.appendChild(Responsavel);
                        tr.appendChild(DiasPVencer);

                        tbody.appendChild(tr);
                    });
                }
            }
        );
    }

    var BtnListar = document.getElementById('listar');
    BtnListar.addEventListener('click', function () {
        AtualizarCads();
        AtualizarGridVencidasAbertas();
        AtualizarGridVencendo();

        setInterval(AtualizarCads, 50000);
        setInterval(AtualizarGridVencidasAbertas, 50000);
        setInterval(AtualizarGridVencendo, 50000);

    }, false);

	$('#EquipeID').selectize({
		plugins: ['remove_button'],
		delimiter: ',',
		persist: false,
		create: function(input) {
			return {
				value: input,
				text: input
			}
		}
	});

	$('#ClienteID').selectize({
		plugins: ['remove_button'],
		delimiter: ',',
		persist: false,
		create: function(input) {
			return {
				value: input,
				text: input
			}
		}
	});

	$('#ProdutoID').selectize({
		plugins: ['remove_button'],
		delimiter: ',',
		persist: false,
		create: function(input) {
			return {
				value: input,
				text: input
			}
		}
    });

});