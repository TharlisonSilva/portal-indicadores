﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            dynamic mymodel = new ExpandoObject();
            mymodel.Equipes = BuscaEquipes();
            mymodel.Clientes = BuscaClientes();
            mymodel.Sistemas = BuscaSistemas();

            return View(mymodel);
        }

        public List<Equipe> BuscaEquipes(){

            var connection = new Conexao().RetornaConexao();
            connection.Open();
            string query = @"SELECT HANDLE, APELIDO
                               FROM SIS_EQUIPES 
                              WHERE INATIVA = 'N'
                              ORDER BY APELIDO";

            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<Equipe>();
                while (reader.Read())    
                    list.Add(new Equipe(int.Parse(reader["HANDLE"].ToString()), reader["APELIDO"].ToString()));

                reader.Close();
                connection.Close();
                return list;
            }
        }
        
        public List<Cliente> BuscaClientes()
        {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT HANDLE, FANTASIA 
                               FROM SIS_CLIENTES 
                              WHERE SUSPENSO = 'N'
                                AND CLIENTEPREMIER = 'S'
                              ORDER BY FANTASIA";

            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<Cliente>();
                while (reader.Read())
                    list.Add(new Cliente(int.Parse(reader["HANDLE"].ToString()), reader["FANTASIA"].ToString()));

                reader.Close();
                connection.Close();
                return list;
            }
        }

        public List<Sistema> BuscaSistemas()
        {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT HANDLE, SISTEMA 
                               FROM SIS_SISTEMAS 
                              WHERE BENNER = 'S' 
                                AND GERARSMSS = 'S'
                              ORDER BY SISTEMA";
            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<Sistema>();
                while (reader.Read())
                    list.Add(new Sistema(int.Parse(reader["HANDLE"].ToString()), reader["SISTEMA"].ToString()));

                reader.Close();
                connection.Close();
                return list;
            }
        }

        public JsonResult ListarSMSVencidasAbertas(string sistemas, string clientes, string equipes)
       {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT A.SMS,
                                    S.SIGLA,
                                    CL.NOME AS CLASSIFICACAO,
                                    CASE WHEN (A.SEVERIDADE = 1) THEN 'MINIMO'
                                         WHEN (A.SEVERIDADE = 2) THEN 'BAIXA'
                                         WHEN (A.SEVERIDADE = 3) THEN 'MÉDIA'
                                         WHEN (A.SEVERIDADE = 4) THEN 'ALTA'
                                         WHEN (A.SEVERIDADE = 5) THEN 'MÁXIMA'
                                         WHEN (A.SEVERIDADE = 6) THEN 'NÃO SE APLICA'
                                    END AS SEVERIDADE,
                                    SI.SITUACAO ,
                                    E.NOME AS EQUIPE,
                                    R.APELIDO AS RESPONSAVEL,
                                    CASE WHEN (A.PRAZOCLIENTE < GETDATE()) THEN 'VENCIDO'
                                         WHEN (A.PRAZOCLIENTE > GETDATE()) THEN 'NÃO VENCEU'
                                    END AS STATUS_SLA,
                                    CAST((GETDATE()-A.PRAZOCLIENTE) AS INT) DIAS_VENCIDO
                               FROM SIS_ATIVIDADES A
                               JOIN SIS_RECURSOS R         ON R.HANDLE = A.RECURSO
                               JOIN SIS_SISTEMAS S         ON S.HANDLE = A.SISTEMA
                               JOIN SIS_CLASSIFICACOES CL  ON CL.HANDLE = A.CLASSIFICACAO
                               JOIN SIS_SITUACOES SI       ON SI.HANDLE = A.SITUACAOATUAL
                               JOIN SIS_EQUIPES E          ON E.HANDLE = R.EQUIPE
                               WHERE A.PRAZOCLIENTE IS NOT NULL
                                 AND A.PRAZOCLIENTE < GETDATE()";
            
            if (sistemas != null && sistemas != "-1")
                query += " AND A.SISTEMA IN (" + sistemas + ")";
            if (clientes != null && clientes != "-1")
                query += " AND A.CLIENTE IN (" + clientes + ")";
            if (equipes != null && equipes != "-1")
                query += " AND E.HANDLE IN (" + equipes + ")";
            
            query += " ORDER BY A.SMS DESC ";

            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<object>();
                while (reader.Read())
                    list.Add(new{
                        Sms = reader["SMS"].ToString(),
                        Sigla = reader["SIGLA"].ToString(),
                        Classificacao = reader["CLASSIFICACAO"].ToString(),
                        Severidade = reader["SEVERIDADE"].ToString(),
                        Situacao = reader["SITUACAO"].ToString(),
                        Equipe = reader["EQUIPE"].ToString(),
                        Responsavel = reader["RESPONSAVEL"].ToString(),
                        StatusSla = reader["STATUS_SLA"].ToString(),
                        DiasVencidos = reader["DIAS_VENCIDO"].ToString()
                    });

                reader.Close();
                connection.Close();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ListarSMSVencendo(string sistemas, string clientes, string equipes)
        {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT A.SMS,
                                    S.SIGLA,
                                    CC.NOME AS CLASSIFICACAO,
                             CASE   WHEN (A.SEVERIDADE = 1) THEN 'MINIMO'
                                    WHEN (A.SEVERIDADE = 2) THEN 'BAIXA'
                                    WHEN (A.SEVERIDADE = 3) THEN 'MÉDIA'
                                    WHEN (A.SEVERIDADE = 4) THEN 'ALTA'
                                    WHEN (A.SEVERIDADE = 5) THEN 'MÁXIMA'
                                    WHEN (A.SEVERIDADE = 6) THEN 'NÃO SE APLICA'
                             END    AS SEVERIDADE,
                                    SI.SITUACAO ,
                                    E.NOME AS EQUIPE,
                                    R.APELIDO AS RESPONSAVEL,
                             CASE   WHEN (A.PRAZOCLIENTE < GETDATE()) THEN 'VENCIDO'
                             	    WHEN (A.PRAZOCLIENTE > GETDATE()) THEN 'NÃO VENCEU'
                             END    AS STATUSSLA,
                                    CAST((A.PRAZOCLIENTE-GETDATE()) AS INT) DIASPARAVENCER
                             FROM SIS_ATIVIDADES A
                             JOIN SIS_CLIENTES C          ON C.HANDLE = A.CLIENTE
                             JOIN SIS_SISTEMAS S          ON S.HANDLE = A.SISTEMA
                             JOIN SIS_CLASSIFICACOES CC   ON CC.HANDLE = A.CLASSIFICACAO
                             JOIN SIS_RECURSOS R          ON R.HANDLE = A.RECURSO
                             JOIN SIS_EQUIPES E           ON E.HANDLE = R.EQUIPE
                             JOIN SIS_SITUACOES SI        ON SI.HANDLE = A.SITUACAOATUAL
                             WHERE A.SITUACAOATUAL NOT IN (2,67,3,8,232)
                               AND A.RECURSO NOT IN (253,252)
                               AND A.PRAZOENTREGA IS NOT NULL
                               AND A.PRAZOCLIENTE <= DATEADD(DD, 10, GETDATE())
                               AND A.PRAZOCLIENTE >= GETDATE()";

            if (sistemas != null && sistemas != "-1")
                query += " AND A.SISTEMA IN (" + sistemas + ")";
            if (clientes != null && clientes != "-1")
                query += " AND A.CLIENTE IN (" + clientes + ")";
            if (equipes != null && equipes != "-1")
                query += " AND E.HANDLE IN (" + equipes + ")";

            query += " ORDER BY A.PRAZOCLIENTE ";

            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<object>();
                while (reader.Read())
                    list.Add(new
                    {
                        Sms = reader["SMS"].ToString(),
                        Sigla = reader["SIGLA"].ToString(),
                        Classificacao = reader["CLASSIFICACAO"].ToString(),
                        Severidade = reader["SEVERIDADE"].ToString(),
                        Situacao = reader["SITUACAO"].ToString(),
                        Equipe = reader["EQUIPE"].ToString(),
                        Responsavel = reader["RESPONSAVEL"].ToString(),
                        StatusSla = reader["STATUSSLA"].ToString(),
                        DiasParaVencer = reader["DIASPARAVENCER"].ToString()
                    });

                reader.Close();
                connection.Close();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CountSMSAbertas(string sistemas, string clientes, string equipes)
       {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT COUNT(A.HANDLE) AS QTD_ABERTAS
                               FROM SIS_ATIVIDADES A
                               JOIN SIS_CLIENTES C        ON C.HANDLE = A.CLIENTE
                               JOIN SIS_SISTEMAS S        ON A.SISTEMA = S.HANDLE
                               JOIN SIS_CLASSIFICACOES CL ON CL.HANDLE = A.CLASSIFICACAO
                               JOIN SIS_RECURSOS R        ON R.HANDLE = A.RECURSO
                               JOIN SIS_SITUACOES SS      ON SS.HANDLE = A.SITUACAOATUAL
                               JOIN SIS_EQUIPES EE        ON EE.HANDLE = R.EQUIPE
                              WHERE A.CLASSIFICACAO NOT IN (4,12)
                                AND A.SITUACAOATUAL NOT IN (2,67,3,8,232)
                                AND A.RECURSO NOT IN (253)";
            
            //----WHERE A.RECURSO IN (SELECT HANDLE FROM SIS_RECURSOS WHERE EQUIPE IN (SELECT HANDLE FROM SIS_EQUIPES WHERE HANDLE = 421))

            if (sistemas != null && sistemas != "-1")
                query += " AND A.SISTEMA IN ("+ sistemas +")";
            if(clientes != null && clientes != "-1")
                query += " AND A.CLIENTE IN ("+ clientes +")";
            if(equipes != null && equipes != "-1")
                query += " AND EE.HANDLE IN (" + equipes +")";
            
            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<object>();
                while (reader.Read())
                    list.Add(new { Quantidade = reader["QTD_ABERTAS"].ToString() });

                reader.Close();
                connection.Close();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
       }

        public JsonResult CountSMSCorrecoes(string sistemas, string clientes, string equipes)
        {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT COUNT(A.HANDLE) AS QTD_CORRECAO
                               FROM SIS_ATIVIDADES A
                               JOIN SIS_CLIENTES C        ON C.HANDLE = A.CLIENTE
                               JOIN SIS_SISTEMAS S        ON A.SISTEMA = S.HANDLE
                               JOIN SIS_CLASSIFICACOES CL ON CL.HANDLE = A.CLASSIFICACAO
                               JOIN SIS_RECURSOS R        ON R.HANDLE = A.RECURSO
                               JOIN SIS_SITUACOES SS      ON SS.HANDLE = A.SITUACAOATUAL
                               JOIN SIS_EQUIPES EE        ON EE.HANDLE = R.EQUIPE
                               WHERE A.CLASSIFICACAO = 1 
                                 AND A.CLASSIFICACAO NOT IN (4,12)
                                 AND A.SITUACAOATUAL NOT IN (2,67,3,8,232)
                                 AND A.RECURSO NOT IN (253)";

            //----WHERE A.RECURSO IN (SELECT HANDLE FROM SIS_RECURSOS WHERE EQUIPE IN (SELECT HANDLE FROM SIS_EQUIPES WHERE HANDLE = 421))

            if (sistemas != null && sistemas != "-1")
                query += " AND A.SISTEMA IN (" + sistemas + ")";
            if (clientes != null && clientes != "-1")
                query += " AND A.CLIENTE IN (" + clientes + ")";
            if (equipes != null && equipes != "-1")
                query += " AND EE.HANDLE IN (" + equipes + ")";

            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<object>();
                while (reader.Read())
                    list.Add(new { Quantidade = reader["QTD_CORRECAO"].ToString() });

                reader.Close();
                connection.Close();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CountNovasSMSAbertasEEncerradas(string sistemas, string clientes, string equipes)
        {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT COALESCE(NOVASSMS.ANO,SMSLIBERADAS.ANO) ANO,
		                            COALESCE(NOVASSMS.MES,SMSLIBERADAS.MES) MES,
		                            COALESCE(CONVERT(VARCHAR, NOVASSMS.DATA, 103),
		                            CONVERT(VARCHAR, SMSLIBERADAS.DATA, 103)) DATA,
		                            COALESCE(NOVASSMS.QUANTIDADE,0) NOVASSMS,
		                            COALESCE(SMSLIBERADAS.QUANTIDADE,0) SMSLIBERADAS,
		                            COALESCE(SMSLIBERADAS.EQUIPE,NOVASSMS.EQUIPE) EQUIPE,
		                            COALESCE(SMSLIBERADAS.SISTEMA,NOVASSMS.SISTEMA) SISTEMA,
		                            COALESCE(SMSLIBERADAS.CLIENTE,NOVASSMS.CLIENTE) CLIENTE
		                            INTO #DADOS
                               FROM ( SELECT COUNT(*) QUANTIDADE, YEAR(DATA) ANO, MONTH(DATA) MES, DATA, EQUIPE, SISTEMA, CLIENTE  FROM (
			                                SELECT A.HANDLE, CAST(A.DATAENTRADA AS DATE) DATA, E.HANDLE EQUIPE , A.SISTEMA, A.CLIENTE
			                                FROM SIS_ATIVIDADES A
		                            INNER JOIN SIS_ATIVIDADEENCAMINHAMENTOS AE ON AE.SMS = A.HANDLE
		                            INNER JOIN Z_GRUPOUSUARIOS U ON U.HANDLE = AE.USUARIOGEROU
		                            INNER JOIN SIS_RECURSOS R ON R.HANDLE = U.RECURSO
		                            INNER JOIN SIS_EQUIPES E ON E.HANDLE = R.EQUIPE
			                                WHERE :xREquipe
				                              AND A.DATAENTRADA >= DATEADD(MM, DATEDIFF(MM,0,GETDATE()) - 1, 0)
		                                GROUP BY A.HANDLE, CAST(A.DATAENTRADA AS DATE), E.HANDLE, A.SISTEMA, A.CLIENTE
                            UNION ALL

                            SELECT A.HANDLE, 
                                    CAST(COALESCE(AEA.ENCAMINHADO,A.DATAENTRADA) AS DATE) DATA, 
	                                E.HANDLE EQUIPE,  
	                                A.SISTEMA, A.CLIENTE
                                FROM SIS_ATIVIDADES A
		                            INNER JOIN SIS_ATIVIDADEENCAMINHAMENTOS AE ON AE.SMS = A.HANDLE
		                            INNER JOIN SIS_ATIVIDADEENCAMINHAMENTOS AEA ON AEA.HANDLE = AE.ENCAMINHAMENTOANTERIOR
		                            INNER JOIN Z_GRUPOUSUARIOS U ON U.HANDLE = AE.USUARIOGEROU
		                            INNER JOIN SIS_RECURSOS R ON R.HANDLE = U.RECURSO
		                            INNER JOIN SIS_EQUIPES E ON E.HANDLE = R.EQUIPE
		                            INNER JOIN (SELECT MIN(AE.HANDLE) HANDLE
					                                FROM SIS_ATIVIDADEENCAMINHAMENTOS AE
				                            INNER JOIN Z_GRUPOUSUARIOS U ON U.HANDLE = AE.USUARIOGEROU
				                            INNER JOIN SIS_RECURSOS R ON R.HANDLE = U.RECURSO
				                                WHERE :xREquipe
				                                GROUP BY AE.SMS) PRIMEIRO_ENCAMINHAMENTO_DESENVOLVIMENTO ON PRIMEIRO_ENCAMINHAMENTO_DESENVOLVIMENTO.HANDLE = AE.HANDLE
			                                WHERE :xREquipe
			                                AND (AEA.ENCAMINHADO >= DATEADD(MM, DATEDIFF(MM,0,GETDATE()) - 1, 0) OR (A.DATAENTRADA >= DATEADD(MM, DATEDIFF(MM,0,GETDATE()) - 1, 0)))
		                                GROUP BY A.HANDLE, CAST(COALESCE(AEA.ENCAMINHADO,A.DATAENTRADA) AS DATE), E.HANDLE,  A.SISTEMA, A.CLIENTE )NOVASSMS
		                            GROUP BY YEAR(DATA), MONTH(DATA), DATA, EQUIPE,  SISTEMA, CLIENTE ) NOVASSMS
		                            FULL JOIN (SELECT COUNT(*) QUANTIDADE, ANO, MES, DATA, EQUIPE,  A.SISTEMA, A.CLIENTE  FROM (
                            SELECT AE.SMS,YEAR(AE.ENCAMINHADO) ANO, 
                                    MONTH(AE.ENCAMINHADO) MES, 
	                                CAST(AE.ENCAMINHADO AS DATE) DATA, 
	                                E.HANDLE EQUIPE,  
	                                A.SISTEMA, 
	                                A.CLIENTE
                                FROM SIS_ATIVIDADEENCAMINHAMENTOS AE
		                            INNER JOIN SIS_ATIVIDADES A ON A.HANDLE = AE.SMS
		                            INNER JOIN SIS_ATIVIDADEENCAMINHAMENTOS AED ON AED.SMS = AE.SMS
		                            INNER JOIN Z_GRUPOUSUARIOS U ON U.HANDLE = AED.USUARIOGEROU
		                            INNER JOIN SIS_RECURSOS R ON R.HANDLE = U.RECURSO
		                            INNER JOIN SIS_EQUIPES E ON E.HANDLE = R.EQUIPE
		                            INNER JOIN (SELECT MIN(AEM.HANDLE) HANDLE, 
                                                       AEM.SMS 
                                                  FROM SIS_ATIVIDADEENCAMINHAMENTOS AEM 
                                                 WHERE AEM.SITUACAO = 67 
                                                 GROUP BY AEM.SMS) PRIMEIRO_HANDLE_HOMOLOGANDO ON PRIMEIRO_HANDLE_HOMOLOGANDO.HANDLE = AE.HANDLE
			                                WHERE :xREquipe
			                                AND AE.SITUACAO = 67
				                                AND AE.ENCAMINHADO >= DATEADD(MM, DATEDIFF(MM,0,GETDATE()) - 1, 0)
		                            GROUP BY AE.SMS,YEAR(AE.ENCAMINHADO), MONTH(AE.ENCAMINHADO), CAST(AE.ENCAMINHADO AS DATE), E.HANDLE,  A.SISTEMA, A.CLIENTE ) A
		                            GROUP BY ANO, MES, DATA, EQUIPE,  A.SISTEMA, A.CLIENTE ) SMSLIBERADAS ON SMSLIBERADAS.DATA = NOVASSMS.DATA AND SMSLIBERADAS.EQUIPE = NOVASSMS.EQUIPE
		                            SELECT SUM(NOVASSMS) AS NOVASSMS ,SUM(SMSLIBERADAS) AS SMSLIBERADAS FROM #DADOS
		                            WHERE MES = MONTH(GETDATE())
		                            :xEquipe
		                            :xSistema
		                            :xCliente
		                            DROP TABLE #DADOS";

           if (equipes != null && equipes != "-1")
            {
                query = query.Replace(":xREquipe", " R.EQUIPE IN("+ equipes + ") ");
                query = query.Replace(":xEquipe", " AND EQUIPE IN("+ equipes + ") ");
            }
            else
            {
                query = query.Replace(":xREquipe", " R.EQUIPE <> 23045  ");  // RECURSO MOLINARE
                query = query.Replace(":xEquipe", " AND EQUIPE <> 23045 ");  // RECURSO MOLINARE
            }

            if (sistemas != null && sistemas != "-1")
                query = query.Replace(":xSistema", " AND SISTEMA IN ("+ sistemas + ") ");
            else
                query = query.Replace(":xSistema", " ");


            if (clientes != null && clientes != "-1")
                query = query.Replace(":xCliente", " AND CLIENTE IN ("+ clientes + ") ");
            else
                query = query.Replace(":xCliente", " ");

            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<object>();
                while (reader.Read())
                    list.Add(new { QuantNovasSMS = reader["NOVASSMS"].ToString(),
                                   QuantSMSLiberadas = reader["SMSLIBERADAS"].ToString() });

                reader.Close();
                connection.Close();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CountSMSVencidasAbertas(string sistemas, string clientes, string equipes)
        {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT COUNT(A.HANDLE) AS QTD_VENCIDAS
                               FROM SIS_ATIVIDADES A  
                               JOIN SIS_RECURSOS R    	   ON R.HANDLE = A.RECURSO
                               JOIN SIS_SISTEMAS S    	   ON S.HANDLE = A.SISTEMA
                               JOIN SIS_CLASSIFICACOES CL  ON CL.HANDLE = A.CLASSIFICACAO
                               JOIN SIS_SITUACOES SI       ON SI.HANDLE = A.SITUACAOATUAL
                               JOIN SIS_EQUIPES E          ON E.HANDLE = R.EQUIPE
                               WHERE A.PRAZOCLIENTE IS NOT NULL
                                 AND A.PRAZOCLIENTE < GETDATE()";

            //--WHERE A.RECURSO IN (SELECT HANDLE FROM SIS_RECURSOS WHERE EQUIPE IN (SELECT HANDLE FROM SIS_EQUIPES WHERE HANDLE = 421))

            if (sistemas != null && sistemas != "-1")
                query += " AND A.SISTEMA IN (" + sistemas + ")";
            if (clientes != null && clientes != "-1")
                query += " AND A.CLIENTE IN (" + clientes + ")";
            if (equipes != null && equipes != "-1")
                query += " AND E.HANDLE IN (" + equipes + ")";

            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<object>();
                while (reader.Read())
                    list.Add(new { Quantidade = reader["QTD_VENCIDAS"].ToString() });

                reader.Close();
                connection.Close();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CountSMSVencendo(string sistemas, string clientes, string equipes)
        {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT Count(A.SMS) AS QNTABERTAS
                             FROM SIS_ATIVIDADES A
                             JOIN SIS_CLIENTES C          ON C.HANDLE = A.CLIENTE
                             JOIN SIS_SISTEMAS S          ON S.HANDLE = A.SISTEMA
                             JOIN SIS_CLASSIFICACOES CC   ON CC.HANDLE = A.CLASSIFICACAO
                             JOIN SIS_RECURSOS R          ON R.HANDLE = A.RECURSO
                             JOIN SIS_EQUIPES E           ON E.HANDLE = R.EQUIPE
                             JOIN SIS_SITUACOES SI        ON SI.HANDLE = A.SITUACAOATUAL
                             WHERE A.SITUACAOATUAL NOT IN (2,67,3,8,232)
                               AND A.RECURSO NOT IN (253,252)
                               AND A.PRAZOENTREGA IS NOT NULL
                               AND A.PRAZOCLIENTE <= DATEADD(DD, 10, GETDATE())
                               AND A.PRAZOCLIENTE >= GETDATE()";

            if (sistemas != null && sistemas != "-1")
                query += " AND A.SISTEMA IN (" + sistemas + ")";
            if (clientes != null && clientes != "-1")
                query += " AND A.CLIENTE IN (" + clientes + ")";
            if (equipes != null && equipes != "-1")
                query += " AND E.HANDLE IN (" + equipes + ")";
            
            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<object>();
                while (reader.Read())
                    list.Add(new{Quantidade = reader["QNTABERTAS"].ToString()});

                reader.Close();
                connection.Close();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult BuscaPercentualSLA(string sistemas, string clientes)
        {
            var connection = new Conexao().RetornaConexao();
            connection.Open();

            string query = @"SELECT AVG(SLA.SLA) SLA,
	                                Concat(MONTH(SLA.PRAZOCLIENTE) , '/',YEAR(SLA.PRAZOCLIENTE) ) as MES
	                           FROM IND_SLA SLA
	                           JOIN SIS_ATIVIDADES SMS ON SMS.HANDLE = SLA.PROTOCOLO
	                           LEFT JOIN SIS_CLIENTESISTEMAS CLIENTESISTEMA ON CLIENTESISTEMA.CLIENTE = SMS.CLIENTE AND CLIENTESISTEMA.SISTEMA = SMS.SISTEMA
	                          WHERE SLA.PRAZOCLIENTE >=  DATEADD(D,1,DATEADD(M,-6,EOMONTH(GETDATE())))
	                            AND SLA.PRAZOCLIENTE <  DATEADD(D,1,DATEADD(M,-0,EOMONTH(GETDATE())))
	                            AND (CLIENTESISTEMA.SLAESPECIFICO = 'S' OR SLA.EHESPECIFICO = 'N')";

            if (sistemas != null && sistemas != "-1")
                query += " AND SMS.SISTEMA IN (" + sistemas + ")";

            if (clientes != null && clientes != "-1")
                query += " AND SLA.CLIENTE IN (" + clientes + ")";

            query += @" GROUP BY YEAR(SLA.PRAZOCLIENTE), MONTH(SLA.PRAZOCLIENTE)
                        ORDER BY YEAR(SLA.PRAZOCLIENTE), MONTH(SLA.PRAZOCLIENTE) ";

            SqlCommand command = new SqlCommand(query, connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                var list = new List<object>();
                while (reader.Read())
                    list.Add(new { Sla = reader["SLA"].ToString(),
                                   Mes = reader["MES"].ToString() });
                

                reader.Close();
                connection.Close();

                return Json(list, JsonRequestBehavior.AllowGet);
            }

        }
    }
}