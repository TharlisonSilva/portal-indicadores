﻿using System.Web;
using System.Web.Optimization;

namespace WebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/Scripts").Include(
                      "~/Scripts/jquery/jquery-3.3.1.min.js",
                      "~/Scripts/bootstrap/bootstrap.js",
                      "~/Scripts/bootstrap/bootstrap.bundle.js",
                      "~/Scripts/slimscroll/jquery.slimscroll.js",
                      "~/Scripts/js/main-js.js",
                      "~/Scripts/charts/chartist-bundle/chartist.min.js",
                      "~/Scripts/charts/sparkline/jquery.sparkline.js",
                      "~/Scripts/charts/morris-bundle/raphael.min.js",
                      "~/Scripts/charts/morris-bundle/morris.js",
                      "~/Scripts/charts/c3charts/c3.min.js",
                      "~/Scripts/charts/c3charts/d3-5.4.0.min.js",
                      "~/Scripts/charts/c3charts/C3chartjs.js",
                      "~/Scripts/libs/dashboard-ecommerce.js",
                      "~/Scripts/libs/selectize.js",
                      "~/Scripts/libs/Custom.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/fonts/circular-std/style.css",
                      "~/Content/style.css",
                      "~/Content/fonts/fontawesome/css/fontawesome-all.css",
                      "~/Content/charts/chartist-bundle/chartist.css",
                      "~/Content/charts/morris-bundle/morris.css",
                      "~/Content/fonts/material-design-iconic-font/css/materialdesignicons.min.css",
                      "~/Content/charts/c3charts/c3.css",
                      "~/Content/fonts/flag-icon-css/flag-icon.min.css",
                      "~/Content/selectize.default.css",
                      "~/Content/Custom.css"));


        }
    }
}
