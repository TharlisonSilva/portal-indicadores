﻿namespace WebApp.Models
{
    public class Cliente
    {
        public int ID { get; set; }
        public string FANTASIA { get; set; }

        public Cliente()
        {
            this.ID = 0;
            this.FANTASIA = "";
        }

        public Cliente(int id, string fantasia)
        {
            this.ID = id;
            this.FANTASIA = fantasia;
        }
    }
}