﻿namespace WebApp.Models
{
    public class Sistema
    {
        public long ID { get; set; }
        public string SISTEMA { get; set; }

        public Sistema()
        {
            this.ID = 0;
            this.SISTEMA = "";
        }

        public Sistema(int id, string nome)
        {
            this.ID = id;
            this.SISTEMA = nome;
        }
    }
}