﻿namespace WebApp.Models
{
    public class Equipe
    {
        public int ID { get; set; }
        public string APELIDO { get; set; }

        public Equipe()
        {
            this.ID = 0;
            this.APELIDO = "";
        }

        public Equipe(int id, string nome)
        {
            this.ID = id;
            this.APELIDO = nome;
        }
    }

}