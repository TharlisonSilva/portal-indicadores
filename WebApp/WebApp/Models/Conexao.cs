﻿using System.Configuration;
using System.Data.SqlClient;

namespace WebApp.Models
{
    public class Conexao
    {
        readonly SqlConnection Con;
        readonly string StringConection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public Conexao()
        {
            this.Con = new SqlConnection(StringConection);
        }
        public SqlConnection RetornaConexao() => this.Con;
    }
}