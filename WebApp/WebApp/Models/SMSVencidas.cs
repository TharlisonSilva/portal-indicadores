﻿namespace WebApp.Models
{
    public class SMSVencidas
    {
        public string NUMEROSMS { get; set; }
        public string SIGLA { get; set; }
        public string CLASSIFICACAO { get; set; }
        public string SEVERIDADE { get; set; }
        public string SITUACAO { get; set; }
        public string EQUIPE { get; set; }
        public string RESPONSAVEL { get; set; }
        public string STATUSSLA { get; set; }
        public string DIASVENCIDO { get; set; }

        public SMSVencidas(string numerosms, string sigla, string classificacao, string severidade, 
                           string situacao, string equipe, string responsavel, string statussla, string diasvencido)
        {
            this.NUMEROSMS = numerosms;
            this.SIGLA = sigla;
            this.CLASSIFICACAO = classificacao;
            this.SEVERIDADE = severidade;
            this.SITUACAO = situacao;
            this.EQUIPE = equipe;
            this.RESPONSAVEL = responsavel;
            this.STATUSSLA = statussla;
            this.DIASVENCIDO = diasvencido;
        }

        public SMSVencidas(){}
    }
}